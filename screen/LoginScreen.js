import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button, Alert } from "react-native";

export default function LoginScreen({ navigation }) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isError, setIsError] = useState(false);

    const submit = () => {
    if (password == '12345678') {
        setIsError(false)
        navigation.navigate('HomeScreen')
      } else {
        setIsError(true)
        Alert.alert(
            "Gagal Login",
            "Please check username and password again"
          [
            {
            text: 'OK',
            onPress: () => { return 'Error Login'}
            }
          ]
        )
      }
    }
  
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 30, fontWeight: "bold", color: 'white' }}>Mom Book Recipe</Text>
      <Image
        style={{ height: 150, width: 150 }}
        source={require("./image/logo.png")}
      />
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Button onPress={submit} title="Login" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#C32F27",
    justifyContent: "center",
    alignItems: "center",
  },
});
