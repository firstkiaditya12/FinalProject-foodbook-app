import React, { Component } from "react";
import {
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";

export default class SplashScreen extends Component {
    componentDidMount() {
      setTimeout(() => {
        this.props.navigation.replace("LoginScreen");
      }, 3000);
    }
  
    render() {
      return (
        <View style={styles.container}>
          <Image
            source={require("./image/logo.png")}
            style={{ width: 150, height: 150 }}
          />
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#C32F27",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "white"
    }
  });
  